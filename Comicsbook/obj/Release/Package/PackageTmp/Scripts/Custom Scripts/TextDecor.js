﻿function insertTag(str)
{
    var text = document.getElementById('textbox');
    var val = text.value;
    var i = getCursorPosition(text);
    text.value = val.slice(0, i) + str + val.slice(i);
}

document.getElementById("b").onclick = function (event)
{
    insertTag("[b][/b]");
}

document.getElementById("i").onclick = function (event) {
    insertTag("[i][/i]");
}

document.getElementById("u").onclick = function (event) {
    insertTag("[u][/u]");
}

document.getElementById("s").onclick = function (event) {
    insertTag("[s][/s]");
}

document.getElementById("img").onclick = function (event) {
    insertTag("[img]Enter URL of your image here[/img]");
}

document.getElementById("video").onclick = function (event) {
    insertTag("[video]Enter URL of your video here[/video]");
}

document.getElementById("url").onclick = function (event) {
    insertTag('[url=http://]Enter name of your link here[/url]');
}

document.getElementById("quote").onclick = function (event) {
    insertTag("[quote]Enter quote here[/quote]");
}

function getCursorPosition(ctrl)
{
    var CaretPos = 0;
    if (document.selection)
    {
        ctrl.focus();
        var Sel = document.selection.createRange();
        Sel.moveStart('character', -ctrl.value.length);
        CaretPos = Sel.text.length;
    }
    else if (ctrl.selectionStart || ctrl.selectionStart == '0')
    {
        CaretPos = ctrl.selectionStart;
    }
    return CaretPos;
}
