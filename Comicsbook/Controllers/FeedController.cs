﻿using Comicsbook.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Comicsbook.Controllers
{
    [HandleError()]
    public class FeedController : Controller
    {
        ApplicationDbContext applicationContext = new ApplicationDbContext();
        // GET: Feed

        [Authorize]
        public ActionResult Feed(int page = 1)
        {
            ViewBag.TagsCloudBlock = GetTagsCloudBlock();

            List<UserTextContent> allContents = applicationContext.Contents.ToList();
            List<UserTextContent> feedContents = new List<UserTextContent>();
            var users = applicationContext.Users;
            var user = users.First(x => x.UserName == User.Identity.Name);

            if (User.Identity.IsAuthenticated)
            {
                ViewBag.CurrentUser = user;
            }

            ViewBag.NeedSidebar = true;

            // Если у юзера не добавлено ни одного тега, не выводим ему статьи.
            if (user.SubscribedTags.Count == 0)
            {
                allContents.Clear();
            }

            var subscribed = user.SubscribedTags.Select(st => st.TagId);
            
            feedContents.AddRange(allContents
                .Where(
                    c => subscribed.Intersect(c.Tags.Select(t => t.TagId)).Count() > 0
                        || applicationContext.SubscribedUsers.Any(su => su.SubscribedAuthorId == c.AuthorId
                            && su.SubscriberId == user.Id)));

            var ignored = user.IgnoredTags.Select(it => it.TagId);
            feedContents.RemoveAll(c => ignored.Intersect(c.Tags.Select(ct => ct.TagId)).Count() > 0);

            // Проверим, что статьи содержат теги, на которые пользователь подписан, и не содержат те, которые он игнорирует.
            // Между проверками добавим все статьи пользователей, у которых данный находится в подписчиках.
            //foreach (var tag in user.SubscribedTags)
            //{
            //    allContents = allContents
            //            .Where(article => article.Tags
            //                .Any(articleTag => articleTag.TagId == tag.TagId))
            //                    .ToList();
            //}

            //var query = from subscription 
            //            in applicationContext.SubscribedUsers 
            //            where subscription.SubscriberId == user.Id
            //            select subscription;
            
            //foreach (var subscribtion in query)
            //{
            //    foreach (var article in users
            //        .First(x => x.Id == subscribtion.SubscribedAuthorId)
            //            .Content)
            //    {
            //        allContents.Add(article);
            //    }
            //}

            //foreach (var tag in user.IgnoredTags)
            //{
            //    allContents = allContents
            //            .Where(article => !article.Tags
            //                .Any(articleTag => articleTag.TagId == tag.TagId))
            //                    .ToList();
            //}

            var ordered = feedContents.OrderByDescending(x => x.Date).Take(100);
            List<UserTextContent> result = new List<UserTextContent>();
            
            for (int i = (page - 1) * 10; i < ordered.Count() && i < page * 10; ++i)
            {
                result.Add(ordered.ElementAt(i));
            }

            ViewBag.currentPage = page;
            ViewBag.pagesCount = (int)Math.Ceiling((float)allContents.Count / 10.0);
            if (result.Count == 0)
            {
                ViewBag.currentPage = -1;
            }
            return View(result);
        }

        public ActionResult Search(int page, int[] tags, int[] ignoredTags, string title, string order = "date")
        {
            ViewBag.TagsCloudBlock = GetTagsCloudBlock();

            //List<Article> articles = (from content in applicationContext.Contents
            //                          where content is Article
            //                          select content as Article).ToList();
            List<UserTextContent> articles = applicationContext.Contents.ToList();
            
            if (tags != null)
            {

                foreach (int tagId in tags)
                {
                    articles = articles
                        .Where(article => article.Tags
                            .Any(articleTag => articleTag.TagId == tagId))
                                .ToList();
                }
                ViewBag.SelectedTags = applicationContext.Tags.Where(x => tags.Contains(x.Id)).ToList();
            }
            else
            {
                ViewBag.SelectedTags = new List<Tag>();
            }

            if (ignoredTags != null)
            {

                foreach (int tagId in ignoredTags)
                {
                    articles = articles
                        .Where(article => !article.Tags
                            .Any(articleTag => articleTag.TagId == tagId))
                                .ToList();
                }
                ViewBag.IgnoredTags = applicationContext.Tags.Where(x => ignoredTags.Contains(x.Id)).ToList();
            }
            else
            {
                ViewBag.IgnoredTags = new List<Tag>();
            }

            if (!string.IsNullOrEmpty(title))
            {
                articles = articles.Where(article => article
                    .Title.ToLower().Contains(title.ToLower())).ToList();
            }

            if (ignoredTags == null)
                tags = new int[0];
            if (tags == null)
                ignoredTags = new int[0];
            ViewBag.ArticleTitle = title ?? "";
            List<UserTextContent> result = new List<UserTextContent>();
            ViewBag.pagesCount = (int)Math.Ceiling((float)articles.Count / 10.0);
            ViewBag.currentPage = page;
            ViewBag.Tags = tags;
            ViewBag.AllTags = applicationContext.Tags.ToList();

            if (order == "date")
            {
                ViewBag.Order = "date";
                articles = articles.OrderByDescending(x => x.Date).ToList();
            }
            else
            {
                ViewBag.Order = "rating";
                articles = articles.OrderByDescending(x => x.Rating).ToList();
            }

            for (int i = (page - 1) * 10; i < articles.Count && i < page * 10; ++i)
            {
                result.Add(articles[i]);
            }
            if (result.Count == 0)
            {
                ViewBag.currentPage = -1;
            }

            if (User.Identity.IsAuthenticated)
            {
                ViewBag.CurrentUser = applicationContext.Users.FirstOrDefault(user => user.UserName == User.Identity.Name);
            }

            ViewBag.NeedSidebar = true;

            return View(result);
        }

        public Dictionary<string, int> GetTagsCloudBlock(int maxTags = 20)
        {
            Dictionary<string, int> result = new Dictionary<string, int>();
            // Counts number of tags added (we only need max of %maxTags% tags).
            int count = 0;
            int total = maxTags < applicationContext.Tags.Count() ? maxTags : applicationContext.Tags.Count();
            ViewBag.Links = new Dictionary<string, int>();
            foreach (var tag in applicationContext.Tags
                        .OrderByDescending(x => x.ContentTags.Count))
            {
                result.Add(tag.Word, count * 5 / (total));
                ViewBag.Links.Add(tag.Word, tag.Id);
                ++count;
                if (count >= maxTags)
                    break;
            }
            return result;
        }
    }
}