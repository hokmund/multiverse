﻿using Comicsbook.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Threading.Tasks;

namespace Comicsbook.Controllers
{
    public class MobileController : Controller
    {
        static MobileController()
        {
            tokens = new Dictionary<string, string>();
        }

        static Dictionary<string, string> tokens;

        ApplicationDbContext applicationContext = new ApplicationDbContext();
        // GET: Mobile
        public ActionResult Feed(int contentCount, string seed)
        {
            List<UserTextContent> fullContent = applicationContext.Contents.ToList();

            //Сортировка контента по дате.
            fullContent.Sort(delegate(UserTextContent c1, UserTextContent c2)
            {
                if (c1.Date < c2.Date) return c1.Id;
                else return c2.Id;
            });

            List<UserTextContent> content = new List<UserTextContent>();
            for (int i = contentCount; i < contentCount + 10 && i < fullContent.Count; i++)
            {
                content.Add(fullContent[i]);
            }

            if (content == null)
            {
                return new EmptyResult();
            }

            List<object> forSer = content.Select(c => ModelConverter.Content(c) as object).ToList();
            Multiverse.Models.Serializable.Response response = new Multiverse.Models.Serializable.Response()
            {
                Info = forSer
            };
            string resultObj = JsonConvert.SerializeObject(response, new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.Objects,
                    TypeNameAssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple
                });

            var r = new Multiverse.Models.Serializable.Response
            {
                Info = forSer
            };         

            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            result.Data = resultObj;
            //result.ExecuteResult(ControllerContext);

            return result;
        }

        public ActionResult GetNextComments(int contentId, int commentNumber, string seed)
        {
            List<Comment> comments = applicationContext.Comments.ToList();
            List<Comment> currentComments = comments.Where(comment => comment.ContentId == contentId).ToList();

            //Сортировка комментариев по дате.
            currentComments.Sort(delegate(Comment c1, Comment c2)
            {
                if (c1.Date < c2.Date) return c1.Id;
                else return c2.Id;
            });

            List<Comment> resultComments = new List<Comment>();
            for (int i = commentNumber; i < commentNumber + 10 && i < currentComments.Count; i++)
            {
                resultComments.Add(currentComments[i]);
            }

            if(resultComments.Count == 0)
            {
                return new EmptyResult();
            }

            List<Multiverse.Models.Serializable.Comment> forSer 
                = resultComments.Select(c => ModelConverter.Comment(c)).ToList();
            Multiverse.Models.Serializable.Response response = new Multiverse.Models.Serializable.Response()
            {
                Info = forSer
            };
            string resultObj = JsonConvert.SerializeObject(response, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Objects,
                TypeNameAssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple
            });

            var r = new Multiverse.Models.Serializable.Response
            {
                Info = forSer
            };

            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            result.Data = resultObj;
            //result.ExecuteResult(ControllerContext);
            return result;
        }

        public ActionResult Convert(int contentId, string seed)
        {
            List<UserTextContent> fullContent = applicationContext.Contents.ToList();
            UserTextContent content = fullContent.First(c => c.Id == contentId);
            Multiverse.Models.Serializable.ExtendedContentInfo extendedContentInfo = ModelConverter.ExtendedContent(content);

            object forSer = ModelConverter.ExtendedContentInfo(extendedContentInfo) as object;
            Multiverse.Models.Serializable.Response response = new Multiverse.Models.Serializable.Response()
            {
                Info = forSer
            };
            string resultObj = JsonConvert.SerializeObject(response, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Objects,
                TypeNameAssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple
            });

            var r = new Multiverse.Models.Serializable.Response
            {
                Info = forSer
            };

            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            result.Data = resultObj;
            //result.ExecuteResult(ControllerContext);
            return result;
        }

        public ActionResult SerchByTitle(string title, string seed)
        {
            List<UserTextContent> fullContent = applicationContext.Contents.ToList();

            // Фильтрует все статьи с данным названием.
            List<UserTextContent> content = fullContent.Where(c => c.Title.Contains(title)).ToList();

            if (content == null)
            {
                return new EmptyResult();
            }

            List<object> forSer = content.Select(c => ModelConverter.Content(c) as object).ToList();
            Multiverse.Models.Serializable.Response response = new Multiverse.Models.Serializable.Response()
            {
                Info = forSer
            };
            string resultObj = JsonConvert.SerializeObject(response, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Objects,
                TypeNameAssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple
            });

            var r = new Multiverse.Models.Serializable.Response
            {
                Info = forSer
            };

            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            result.Data = resultObj;
            //result.ExecuteResult(ControllerContext);

            return result;
        }

        public ActionResult SerchByTag(string tag, string seed)
        {
            List<UserTextContent> fullContent = applicationContext.Contents.ToList();

            // Оставляет все статьи, содержащие данный тег.
            List<UserTextContent> content = fullContent.Where(c => c.Tags.Count(t => t.Tag.Word == tag) > 0).ToList();

            if (content == null)
            {
                return new EmptyResult();
            }

            List<object> forSer = content.Select(c => ModelConverter.Content(c) as object).ToList();
            Multiverse.Models.Serializable.Response response = new Multiverse.Models.Serializable.Response()
            {
                Info = forSer
            };
            string resultObj = JsonConvert.SerializeObject(response, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Objects,
                TypeNameAssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple
            });

            var r = new Multiverse.Models.Serializable.Response
            {
                Info = forSer
            };

            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            result.Data = resultObj;
            //result.ExecuteResult(ControllerContext);

            return result;
        }

        public ActionResult GetUser(string username, string seed)
        {
            ApplicationUser user = applicationContext.Users.FirstOrDefault(u => u.UserName == username);
            Multiverse.Models.Serializable.User forSer = ModelConverter.User(user);

            Multiverse.Models.Serializable.Response response = new Multiverse.Models.Serializable.Response()
            {
                Info = forSer
            };
            string resultObj = JsonConvert.SerializeObject(response, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Objects,
                TypeNameAssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple
            });

            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            result.Data = resultObj;

            return result;
        }

        public async Task<ActionResult> Login(string username, string password)
        {
            var user = applicationContext.Users.FirstOrDefault(u => u.UserName == username);

            string responseMessage;
            JsonResult result = new JsonResult();

            if(user == null)
            {
                responseMessage = "Error";
            }
            else
            {
                SignInStatus loginResult;
                try
                {
                    loginResult = await HttpContext.GetOwinContext().Get<ApplicationSignInManager>().PasswordSignInAsync(username, password, false, false);
                }
                catch
                {
                    responseMessage = "Error";
                    Multiverse.Models.Serializable.Response r = new Multiverse.Models.Serializable.Response
                    {
                        Info = responseMessage
                    };
                    result.Data = JsonConvert.SerializeObject(r);
                    return result;
                }
                
                if (loginResult == SignInStatus.Success)
                {
                    responseMessage = "OK";
                    setToken(username);
                }
                else
                {
                    responseMessage = "Error";
                }
            }

            Multiverse.Models.Serializable.Response response = new Multiverse.Models.Serializable.Response
            {
                Info = responseMessage,
                ValidationToken = tokens[username]
            };
            result.Data = JsonConvert.SerializeObject(response);
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            return result;
        }

        public ActionResult Logoff(string username)
        {
            if (tokens.ContainsKey(username))
            {
                tokens.Remove(username);
            }

            return new EmptyResult();
        }

        public ActionResult AddComment(int contentId, string commentText)
        {
            Multiverse.Models.Serializable.Response response = new Multiverse.Models.Serializable.Response();

            var content = applicationContext.Contents.FirstOrDefault(c => c.Id == contentId);
            if (content == null)
            {
                response.Info = "Error";
            }
            else
            {
                var username = Request.Cookies["username"].Value;
                var token = Request.Cookies["token"].Value;

                if (tokens[username] != token)
                {
                    response.Info = "Error";
                }
                else
                {
                    var user = applicationContext.Users.FirstOrDefault(u => u.UserName == username);
                    applicationContext.Comments.Add(new Comment
                        {
                            Author = user,
                            AuthorId = user.Id,
                            CommentText = commentText,
                            Content = content,
                            ContentId = content.Id,
                            Date = DateTime.Now,                            
                        });
                    applicationContext.SaveChanges();
                    response.Info = "OK";
                    setToken(username);
                    response.ValidationToken = tokens[username];
                }
            }


            string resultObj = JsonConvert.SerializeObject(response, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Objects,
                TypeNameAssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple
            });

            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            result.Data = resultObj;

            return result;
        }

        [NonAction]
        void setToken(string username)
        {
            string token = Guid.NewGuid().ToString();
            if (!tokens.ContainsKey(username))
            {
                tokens.Add(username, token);
            }
            else
            {
                tokens[username] = token;
            }
        }
    }
}