﻿using System.Linq;
using System.Web.Mvc;
using Comicsbook.Models;
using System.Collections.Generic;

namespace Comicsbook.Controllers
{
    [HandleError()]
    public class CalendarController : Controller
    {
        ApplicationDbContext applicationContext = new ApplicationDbContext();

        // GET: Calendar
        public ActionResult ShowInitialCalendar()
        {
            ViewBag.TagsCloudBlock = GetTagsCloudBlock();
            ApplicationUser currentUser = applicationContext.Users.First(user => user.UserName == User.Identity.Name);
            ViewData["User"] = currentUser;
            ViewBag.CurrentUser = currentUser;
            ViewBag.NeedSidebar = true;
            return View();
        }

        [HttpPost]
        public ActionResult ShowPartialCalendar(int month, int year)
        {
            ViewBag.TagsCloudBlock = GetTagsCloudBlock();
            ApplicationUser currentUser = applicationContext.Users.First(user => user.UserName == User.Identity.Name);
            ViewData["User"] = currentUser;
            ViewData["Month"] = month;
            ViewData["Year"] = year;
            return PartialView("CalendarPartial");
        }

        public Dictionary<string, int> GetTagsCloudBlock(int maxTags = 20)
        {
            Dictionary<string, int> result = new Dictionary<string, int>();
            // Counts number of tags added (we only need max of %maxTags% tags).
            int count = 0;
            int total = maxTags < applicationContext.Tags.Count() ? maxTags : applicationContext.Tags.Count();
            ViewBag.Links = new Dictionary<string, int>();
            foreach (var tag in applicationContext.Tags
                        .OrderByDescending(x => x.ContentTags.Count))
            {
                result.Add(tag.Word, count * 5 / (total));
                ViewBag.Links.Add(tag.Word, tag.Id);
                ++count;
                if (count >= maxTags)
                    break;
            }
            return result;
        }
    }
}