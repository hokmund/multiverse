﻿using Comicsbook.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Comicsbook.Controllers
{
    [HandleError()]
    public class UserContentController : Controller
    {
        ApplicationDbContext applicationContext = new ApplicationDbContext();

        [HttpGet]
        public ActionResult Index()
        {
            //List<Article> articles = new List<Article>();
            //lock (applicationContext.locker)
            //{
            //    articles = applicationContext.Articles.ToList();
            //    ViewBag.Users = applicationContext.Users.ToList();
            //}

            //return View(articles);

            ///for future - RedirectToAction("Index", "Feed", ...)

            ViewBag.TagsCloudBlock = GetTagsCloudBlock();
            return new EmptyResult();
        }

        public ActionResult Articles()
        {
            if (User.Identity.IsAuthenticated)
            {
                ViewBag.CurrentUser = applicationContext.Users.FirstOrDefault(user => user.UserName == User.Identity.Name);
            }

            ViewBag.NeedSidebar = true;
            ViewBag.TagsCloudBlock = GetTagsCloudBlock();
            return View(applicationContext.Articles.ToList().OrderByDescending(x => x.Date));
        }

        public ActionResult Events()
        {
            if (User.Identity.IsAuthenticated)
            {
                ViewBag.CurrentUser = applicationContext.Users.FirstOrDefault(user => user.UserName == User.Identity.Name);
            }

            ViewBag.NeedSidebar = true;
            ViewBag.TagsCloudBlock = GetTagsCloudBlock();
            return View(applicationContext.Events.ToList().OrderByDescending(x => x.Date));
        }

        public ActionResult Meetings()
        {
            if (User.Identity.IsAuthenticated)
            {
                ViewBag.CurrentUser = applicationContext.Users.FirstOrDefault(user => user.UserName == User.Identity.Name);
            }

            ViewBag.NeedSidebar = true;
            ViewBag.TagsCloudBlock = GetTagsCloudBlock();
            return View(applicationContext.Meetings.ToList().OrderByDescending(x => x.Date));
        }

        [ActionName("article")]
        public ActionResult Article(int id)
        {
            ViewBag.TagsCloudBlock = GetTagsCloudBlock();
            return RedirectToAction("Show", new { id = id });
        }

        [ActionName("event")]
        public ActionResult Event(int id)
        {
            ViewBag.TagsCloudBlock = GetTagsCloudBlock();
            return RedirectToAction("Show", new { id = id });
        }

        [ActionName("meeting")]
        public ActionResult Meeting(int id)
        {
            ViewBag.TagsCloudBlock = GetTagsCloudBlock();
            return RedirectToAction("Show", new { id = id });
        }

        [AllowAnonymous]
        public ActionResult Show(int id)
        {
            ViewBag.TagsCloudBlock = GetTagsCloudBlock();
            ApplicationUser currentUser = applicationContext.Users.FirstOrDefault(user => user.UserName == User.Identity.Name);
            var selected = applicationContext.Contents.FirstOrDefault(content => content.Id == id);

            if (selected == null)
            {
                return new HttpNotFoundResult();
            }

            ViewBag.Type = selected.Type;

            ViewBag.NeedSidebar = true;

            selected.Comments = applicationContext.Comments.Where(comment => comment.ContentId == id).ToList();

            ViewBag.Comments = applicationContext.Comments.Where(comment => comment.ContentId == id).ToList();
            ViewBag.Users = applicationContext.Users.ToList();
            ViewBag.CurrentUser = currentUser;
            return View(selected);
        }


        [NonAction]
        private List<Tag> GetTags()
        {
            ViewBag.TagsCloudBlock = GetTagsCloudBlock();
            // Сортировка тегов по количеству их упоминаний в статьях.
            List<Tag> tags = new List<Tag>();
            List<Tag> existingTags = applicationContext.Tags.ToList();

            while (existingTags.Count() > 0)
            {
                // Нахождение максисального элемента.
                Tag maxTag = existingTags.ElementAt(0);
                for (int i = 1; i < existingTags.Count(); i++)
                {
                    Tag existingTag = existingTags.ElementAt(i);
                    if (existingTag.ContentTags.Count > maxTag.ContentTags.Count)
                    {
                        maxTag = existingTag;
                    }
                }
                // Добавление максимального элемента в новую последовательность.
                tags.Add(maxTag);
                // Удаление элемента из старой последовательности.
                existingTags.Remove(maxTag);
            }
            return tags;
        }


        [Authorize]
        public ActionResult New(ContentType type)
        {
            if (User.Identity.IsAuthenticated)
            {
                ViewBag.CurrentUser = applicationContext.Users.FirstOrDefault(user => user.UserName == User.Identity.Name);
            }

            ViewBag.NeedSidebar = true;
            ViewBag.TagsCloudBlock = GetTagsCloudBlock();
            ViewBag.Tags = GetTags();
            ViewBag.NewType = type;
            return View();
        }

        //[ValidateInput(false)]
        [Authorize]
        [HttpPost]
        public ActionResult New(string Title, string Text, string[] tags, string longtitude = "", string lattitude = "", DateTime? eventDate = null,
            ContentType type = ContentType.Article)
        {
            ViewBag.TagsCloudBlock = GetTagsCloudBlock();
            string authorId = applicationContext.Users.First(user => user.UserName == User.Identity.Name).Id;
            UserTextContent content = null;
            if (type == ContentType.Article)
            {
                content = new Article()
                {
                    Date = DateTime.Now,
                    AuthorId = authorId,
                    Title = Title,
                    Text = Text,
                };
            }
            if (type == ContentType.Event)
            {
                content = new Event()
                {
                    Date = DateTime.Now,
                    AuthorId = authorId,
                    Title = Title,
                    Text = Text,
                    EventDate = eventDate
                };
            }
            if (type == ContentType.Meeting)
            {
                content = new Meeting()
                {
                    Date = DateTime.Now,
                    AuthorId = authorId,
                    Title = Title,
                    Text = Text,
                    EventDate = eventDate,
                    GeoLong = longtitude,
                    GeoLat = lattitude
                };
            }

            if (content == null)
            {
                return new EmptyResult();
            }

            if (Title != string.Empty && Text != string.Empty && ModelState.IsValid)
            {
                if (tags == null)
                {
                    tags = new string[0];
                }

                // Добавляем теги
                for (int i = 0; i < tags.Length; i++)
                {
                    string word = tags[i];
                    Tag existingTag = applicationContext.Tags.FirstOrDefault(tag => tag.Word == word);
                    if (existingTag != null && existingTag.Word != "" && existingTag.Word != null)
                    {
                        ContentTag newContentTag = new ContentTag();
                        newContentTag.Content = content;
                        newContentTag.Tag = existingTag;
                        applicationContext.ContentTags.Add(newContentTag);

                        content.Tags.Add(newContentTag);
                    }
                    else
                    {
                        ContentTag newContentTag = new ContentTag();
                        newContentTag.Content = content;

                        Tag newTag = new Tag();
                        newTag.Id = applicationContext.Tags.Count() + 1;
                        newTag.Word = tags[i];
                        newTag.ContentTags.Add(newContentTag);

                        newContentTag.Tag = newTag;

                        content.Tags.Add(newContentTag);

                        applicationContext.Tags.Add(newTag);
                        applicationContext.ContentTags.Add(newContentTag);
                    }
                }

                applicationContext.Contents.Add(content);
                applicationContext.SaveChanges();
                int tempId = content.Id;
                return RedirectToAction("Show", new { id = tempId });
            }
            ViewBag.TagsCloudBlock = GetTagsCloudBlock();
            ViewBag.Tags = GetTags();
            ViewBag.NewType = type;
            if (User.Identity.IsAuthenticated)
            {
                ViewBag.CurrentUser = applicationContext.Users.FirstOrDefault(user => user.UserName == User.Identity.Name);
            }

            ViewBag.NeedSidebar = true;
            return View(content);
        }

        [ValidateInput(false)]
        public ActionResult Edit(UserTextContent content)
        {
            ViewBag.TagsCloudBlock = GetTagsCloudBlock();
            string id = Request.Form.Get("id") ?? content.Id.ToString();
            UserTextContent editedContent = applicationContext.Contents.FirstOrDefault(
                edited => edited.Id.ToString() == id);
            if (editedContent == null)
            {
                return new EmptyResult();
            }
            //string newText = Request.Form.Get("Text");
            string newText = null;
            if (content != null)
            {
                newText = content.Text;
            }
            if (newText != null && editedContent.Text != newText)
            {
                editedContent.Text = newText;//.Replace("\r\n", "<br />").Replace("\n", "<br />");
                applicationContext.SaveChanges();
            }
            if (User.Identity.IsAuthenticated)
            {
                ViewBag.CurrentUser = applicationContext.Users.FirstOrDefault(user => user.UserName == User.Identity.Name);
            }

            ViewBag.NeedSidebar = true;

            return PartialView(editedContent);
        }

        [ValidateInput(false)]
        public ActionResult EditArticle(Article article)
        {
            ViewBag.TagsCloudBlock = GetTagsCloudBlock();
            string id = Request.Form.Get("ArticleId") ?? article.Id.ToString();
            Article editedArticle = applicationContext.Contents.FirstOrDefault(
                edited => edited.Id.ToString() == id) as Article;
            if (editedArticle == null)
            {
                return new EmptyResult();
            }
            //string newText = Request.Form.Get("Text");
            string newText = null;
            if (article != null)
            {
                newText = article.Text;
            }
            if (newText != null && editedArticle.Text != newText)
            {
                editedArticle.Text = newText;//.Replace("\r\n", "<br />").Replace("\n", "<br />");
                applicationContext.SaveChanges();
            }
            if (User.Identity.IsAuthenticated)
            {
                ViewBag.CurrentUser = applicationContext.Users.FirstOrDefault(user => user.UserName == User.Identity.Name);
            }

            ViewBag.NeedSidebar = true;

            return PartialView(editedArticle);
        }

        [Authorize]
        public ActionResult AddMark(MarkValue mark, int contentId)
        {
            ViewBag.TagsCloudBlock = GetTagsCloudBlock();
            UserTextContent markedContent = applicationContext.Contents.FirstOrDefault(
                content => content.Id == contentId);

            if (markedContent == null)
            {
                return new EmptyResult();
            }

            ViewBag.Like = new bool?();
            ViewBag.Like = null;

            if (User.Identity.IsAuthenticated && markedContent.Author.UserName != User.Identity.Name)
            {
                var currentUser = applicationContext.Users.FirstOrDefault(
                    user => user.UserName == User.Identity.Name);

                ViewBag.Like = mark == MarkValue.Like ? true : false;
                applicationContext.ContentMarks.Add(new ContentMark
                {
                    Content = markedContent,
                    Author = currentUser,
                    ContentId = markedContent.Id,
                    AuthorId = currentUser.Id,
                    Value = mark
                });
                applicationContext.SaveChanges();
            }

            return PartialView("CreateRatingBlock", markedContent);
        }

        [Authorize]
        public ActionResult RemoveMark(MarkValue mark, int contentId)
        {
            ViewBag.TagsCloudBlock = GetTagsCloudBlock();
            UserTextContent unmarkedContent = applicationContext.Contents.FirstOrDefault(
                content => content.Id == contentId);

            if (unmarkedContent == null)
            {
                return new EmptyResult();
            }

            ViewBag.Like = new bool?();
            ViewBag.Like = null;

            var currentUser = applicationContext.Users.FirstOrDefault(
                user => user.UserName == User.Identity.Name);
            var deletingMark = unmarkedContent.Marks.First(
                mark1 => mark1.AuthorId == currentUser.Id);

            if (User.Identity.IsAuthenticated
                && unmarkedContent.Author.UserName != User.Identity.Name)
            {
                applicationContext.ContentMarks.Remove(deletingMark);
                applicationContext.SaveChanges();
            }

            if (deletingMark.Value != mark)
            {
                return AddMark(mark, contentId);
            }

            return PartialView("CreateRatingBlock", unmarkedContent);
        }

        [ChildActionOnly]
        public ActionResult CreateRatingBlock(UserTextContent content)
        {
            ViewBag.TagsCloudBlock = GetTagsCloudBlock();
            var articleAuthor = applicationContext.Users.FirstOrDefault(user => user.Id == content.AuthorId);
            var currentUser = applicationContext.Users.FirstOrDefault(user => user.UserName == User.Identity.Name);

            ViewBag.Like = new bool?();
            ViewBag.Like = null;
            if (currentUser != null && articleAuthor != currentUser)
            {
                var thisUserMark = content.Marks.FirstOrDefault(mark => mark.AuthorId == currentUser.Id);
                if (thisUserMark != default(ContentMark))
                {
                    switch (thisUserMark.Value)
                    {
                        case MarkValue.Dislike:
                            ViewBag.Like = false;
                            break;
                        case MarkValue.Like:
                            ViewBag.Like = true;
                            break;
                        default:
                            break;
                    }
                }
            }
            return PartialView(content);

        }

        [ChildActionOnly]
        [Authorize]
        public ActionResult AddComment(int id)
        {
            ViewBag.TagsCloudBlock = GetTagsCloudBlock();
            return PartialView(new Comment { ContentId = id });
        }

        [HttpPost]
        [Authorize]
        public ActionResult AddComment(Comment comment)
        {
            ViewBag.TagsCloudBlock = GetTagsCloudBlock();
            ApplicationUser author = applicationContext.Users.First(user => user.UserName == User.Identity.Name);
            UserTextContent content = applicationContext.Contents.FirstOrDefault(art => art.Id == comment.ContentId);

            if (content == null)
            {
                return new EmptyResult();
            }

            Comment toContext = new Comment
            {
                Content = content,
                ContentId = comment.ContentId,
                Author = author,
                AuthorId = author.Id,
                CommentAnswerId = comment.CommentAnswerId,
                Date = DateTime.Now,
                CommentText = comment.CommentText
            };
            applicationContext.Comments.Add(toContext);
            applicationContext.SaveChanges();
            //var allComments = applicationContext.Comments.Where(comm => comm.ArticleId == article.ArticleId).ToList();
            if (comment.CommentAnswerId != null)
            {
                string authorId = applicationContext.Comments.First(comm => comm.Id == comment.CommentAnswerId).AuthorId;
                ViewBag.ReplyAuthor = applicationContext.Users.First(user => user.Id == authorId);
            }


            return PartialView("ShowComment", toContext);
        }

        //[ChildActionOnly]
        public ActionResult CreateCommentRating(Comment comment)
        {
            ViewBag.TagsCloudBlock = GetTagsCloudBlock();
            ViewBag.CommentLike = new bool?();
            ViewBag.CommentLike = null;

            CommentMark userMark = applicationContext.CommentMarks.FirstOrDefault(mark => mark.Author.UserName == User.Identity.Name
                && mark.CommentId == comment.Id);

            if (userMark != null)
            {
                ViewBag.CommentLike = userMark.Value == MarkValue.Like ? true : false;
            }

            return PartialView(comment);
        }

        [Authorize]
        public ActionResult AddCommentMark(MarkValue mark, int commentId)
        {
            ViewBag.TagsCloudBlock = GetTagsCloudBlock();
            Comment markedComment = applicationContext.Comments.FirstOrDefault(comment => comment.Id == commentId);

            if (markedComment == null)
            {
                return new EmptyResult();
            }

            ApplicationUser author = applicationContext.Users.First(user => user.UserName == User.Identity.Name);

            if (markedComment.Marks.FirstOrDefault(comm => comm.AuthorId == author.Id) != null
                || markedComment.AuthorId == author.Id)
            {
                return new EmptyResult();
            }

            CommentMark commentMark = new CommentMark
            {
                //Author = author,
                AuthorId = author.Id,
                //Comment = markedComment,
                CommentId = markedComment.Id,
                Value = mark
            };
            //author.CommentMarks.Add(commentMark);

            //List<CommentMark> newMarks = new List<CommentMark>(markedComment.Marks);
            //newMarks.Add(commentMark);
            //markedComment.Marks = newMarks;

            applicationContext.CommentMarks.Add(commentMark);

            applicationContext.SaveChanges();

            markedComment = applicationContext.Comments.FirstOrDefault(comment => comment.Id == commentId);
            ViewBag.CommentLike = commentMark.Value == MarkValue.Like ? true : false;

            return PartialView("CreateCommentRating", markedComment);
        }

        [HttpPost]
        public ActionResult SubscribeAtTag(int tagId, int contentId)
        {
            ViewBag.TagsCloudBlock = GetTagsCloudBlock();
            ApplicationUser currentUser = applicationContext.Users.First(user => user.UserName == User.Identity.Name);
            SubscribedTag subscribedTag = new SubscribedTag { TagId = tagId, Tag = applicationContext.Tags.First(tag => tag.Id== tagId), UserId = currentUser.Id, User = currentUser };
            if (currentUser.SubscribedTags.Count(tag => tag.TagId == tagId) == 0)
            {
                applicationContext.SubscribedTags.Add(subscribedTag);

                IgnoredTag ignoredTag = new IgnoredTag { TagId = tagId, Tag = applicationContext.Tags.First(tag => tag.Id == tagId), UserId = currentUser.Id, User = currentUser };
                if (applicationContext.IgnoredTags.Count(st => st.TagId == ignoredTag.TagId && st.UserId == ignoredTag.UserId) > 0)
                    applicationContext.IgnoredTags.Remove(applicationContext.IgnoredTags.First(st => st.TagId == ignoredTag.TagId && st.UserId == ignoredTag.UserId));
            }
            else
            {
                applicationContext.SubscribedTags.Remove(applicationContext.SubscribedTags.First(st => st.TagId == subscribedTag.TagId && st.UserId == subscribedTag.UserId));
            }
            applicationContext.SaveChanges();

            var selected = applicationContext.Contents.FirstOrDefault(content => content.Id == contentId);

            ViewBag.CurrentUser = currentUser;
            ViewData["ContentId"] = contentId;
            return PartialView("TagListMenuPartial", selected);
        }

        [HttpPost]
        public ActionResult IgnoreTag(int tagId, int contentId)
        {
            ViewBag.TagsCloudBlock = GetTagsCloudBlock();
            ApplicationUser currentUser = applicationContext.Users.First(user => user.UserName == User.Identity.Name);
            IgnoredTag ignoredTag = new IgnoredTag { TagId = tagId, Tag = applicationContext.Tags.First(tag => tag.Id == tagId), UserId = currentUser.Id, User = currentUser };
            if (currentUser.IgnoredTags.Count(tag => tag.TagId == tagId) == 0)
            {
                applicationContext.IgnoredTags.Add(ignoredTag);

                SubscribedTag subscribedTag = new SubscribedTag { TagId = tagId, Tag = applicationContext.Tags.First(tag => tag.Id == tagId), UserId = currentUser.Id, User = currentUser };
                if (applicationContext.SubscribedTags.Count(st => st.TagId == subscribedTag.TagId && st.UserId == subscribedTag.UserId) > 0)
                    applicationContext.SubscribedTags.Remove(applicationContext.SubscribedTags.First(st => st.TagId == subscribedTag.TagId && st.UserId == subscribedTag.UserId));
            }
            else
            {
                applicationContext.IgnoredTags.Remove(applicationContext.IgnoredTags.First(st => st.TagId == ignoredTag.TagId && st.UserId == ignoredTag.UserId));
            }
            applicationContext.SaveChanges();

            var selected = applicationContext.Contents.FirstOrDefault(content => content.Id == contentId);
            
            ViewBag.CurrentUser = currentUser;
            ViewData["ContentId"] = contentId;
            return PartialView("TagListMenuPartial", selected);
        }

        [HttpPost]
        public ActionResult SubscribeAtEvent(int eventId)
        {
            ViewBag.TagsCloudBlock = GetTagsCloudBlock();
            ApplicationUser currentUser = applicationContext.Users.First(user => user.UserName == User.Identity.Name);
            SubscribedEvent subscribedEvent = new SubscribedEvent { EventId = eventId, Event = applicationContext.Events.First(ev => ev.Id == eventId), UserId = currentUser.Id, User = currentUser };
            applicationContext.SubscribedEvents.Add(subscribedEvent);
            applicationContext.SaveChanges();

            var selected = applicationContext.Contents.FirstOrDefault(content => content.Id == eventId);

            ViewData["ContentId"] = eventId;
            ViewData["CurrentUser"] = currentUser;
            return PartialView("EventButtonPartial", selected);
        }

        [HttpPost]
        public ActionResult UnsubscribeFromEvent(int eventId)
        {
            ViewBag.TagsCloudBlock = GetTagsCloudBlock();
            ApplicationUser currentUser = applicationContext.Users.First(user => user.UserName == User.Identity.Name);
            SubscribedEvent subscribedEvent = new SubscribedEvent { EventId = eventId, Event = applicationContext.Events.First(ev => ev.Id == eventId), UserId = currentUser.Id, User = currentUser };
            applicationContext.SubscribedEvents.Remove(applicationContext.SubscribedEvents.First(se => se.EventId == subscribedEvent.EventId && se.UserId == subscribedEvent.UserId));
            applicationContext.SaveChanges();

            var selected = applicationContext.Contents.FirstOrDefault(content => content.Id == eventId);

            ViewData["ContentId"] = eventId;
            ViewData["CurrentUser"] = currentUser;
            return PartialView("EventButtonPartial", selected);
        }

        [HttpPost]
        public ActionResult SubscribeAtMeeting(int meetingId)
        {
            ViewBag.TagsCloudBlock = GetTagsCloudBlock();
            ApplicationUser currentUser = applicationContext.Users.First(user => user.UserName == User.Identity.Name);
            SubscribedMeeting subscribedMeeting = new SubscribedMeeting { MeetingId = meetingId, Meeting = applicationContext.Meetings.First(m => m.Id == meetingId), UserId = currentUser.Id, User = currentUser };
            applicationContext.SubscribedMeetings.Add(subscribedMeeting);
            applicationContext.SaveChanges();

            var selected = applicationContext.Contents.FirstOrDefault(content => content.Id == meetingId);

            ViewData["ContentId"] = meetingId;
            ViewData["CurrentUser"] = currentUser;
            return PartialView("MeetingButtonPartial", selected);
        }

        [HttpPost]
        public ActionResult UnsubscribeFromMeeting(int meetingId)
        {
            ViewBag.TagsCloudBlock = GetTagsCloudBlock();
            ApplicationUser currentUser = applicationContext.Users.First(user => user.UserName == User.Identity.Name);
            SubscribedMeeting subscribedMeeting = new SubscribedMeeting { MeetingId = meetingId, Meeting = applicationContext.Meetings.First(ev => ev.Id == meetingId), UserId = currentUser.Id, User = currentUser };
            applicationContext.SubscribedMeetings.Remove(applicationContext.SubscribedMeetings.First(se => se.MeetingId == subscribedMeeting.MeetingId && se.UserId == subscribedMeeting.UserId));
            applicationContext.SaveChanges();

            var selected = applicationContext.Contents.FirstOrDefault(content => content.Id == meetingId);

            ViewData["ContentId"] = meetingId;
            ViewData["CurrentUser"] = currentUser;
            return PartialView("MeetingButtonPartial", selected);
        }


        public Dictionary<string, int> GetTagsCloudBlock(int maxTags = 20)
        {
            Dictionary<string, int> result = new Dictionary<string, int>();
            // Counts number of tags added (we only need max of %maxTags% tags).
            int count = 0;
            int total = maxTags < applicationContext.Tags.Count() ? maxTags : applicationContext.Tags.Count();
            ViewBag.Links = new Dictionary<string, int>();
            foreach (var tag in applicationContext.Tags
                        .OrderByDescending(x => x.ContentTags.Count))
            {
                result.Add(tag.Word, count * 5 / (total));
                ViewBag.Links.Add(tag.Word, tag.Id);
                ++count;
                if (count >= maxTags)
                    break;
            }
            return result;
        }

        [HttpPost]
        public ActionResult Edit(int id)
        {
            ViewBag.TagsCloudBlock = GetTagsCloudBlock();
            UserTextContent editedContent = applicationContext.Contents.FirstOrDefault(
                edited => edited.Id == id);
            if (editedContent == null)
            {
                return new EmptyResult();
            }

            string newText = null;

            newText = Request.Form.Get("Text");
            if (newText != null && editedContent.Text != newText)
            {
                editedContent.Text = newText;
                applicationContext.SaveChanges();
            }

            return PartialView(editedContent);
        }
    }
}