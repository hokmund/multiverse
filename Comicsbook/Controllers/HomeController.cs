﻿using System.Web.Mvc;
using Comicsbook.Models;
using System.Collections.Generic;
using System.Linq;

namespace Comicsbook.Controllers
{
    [HandleError()]
    public class HomeController : Controller
    {
        ApplicationDbContext applicationContext = new ApplicationDbContext();

        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.TagsCloudBlock = GetTagsCloudBlock();
            return View();
        }

        [HttpGet]
        public ActionResult TagsCloud()
        {
            int maxTags = 100;
            ViewBag.TagsCloudBlock = GetTagsCloudBlock();
            var result = GetTagsCloudBlock(maxTags);
            if (User.Identity.IsAuthenticated)
            {
                var user = applicationContext.Users.First(x => x.UserName == User.Identity.Name);
                ViewBag.Recommends = checkTagsCompatibility(user);
                ViewBag.CurrentUser = user;
            }
            ViewBag.NeedTags = false;
            return View(result);
        }

        private List<Tag> checkTagsCompatibility(ApplicationUser user)
        {
            List<Tag> result = new List<Tag>();
            Dictionary<Tag, int> compatibility = new Dictionary<Tag, int>();
            foreach (var tag in user.SubscribedTags)
            {
                var subscribers = new List<ApplicationUser>();
                foreach (var u in applicationContext.Users)
                    if (u.SubscribedTags.Any(t => t.Tag == tag.Tag))
                        subscribers.Add(u);
                foreach (var subscriber in subscribers)
                {
                    foreach (var subtag in subscriber.SubscribedTags.ToList())
                    {
                        if (compatibility.ContainsKey(subtag.Tag))
                            compatibility[subtag.Tag]++;
                        else
                            compatibility[subtag.Tag] = 1;
                    }
                }
            }
            result = compatibility.OrderByDescending(x => x.Value)
                .Where(t => !user.SubscribedTags.Any(q => q.Tag == t.Key))
                .Select(p => p.Key).Take(10).ToList();
            ViewBag.RecommendsLinks = new Dictionary<string, int>();
            foreach (var t in result)
                ViewBag.RecommendsLinks.Add(t.Word, t.Id);
            return result;
        }

        public Dictionary<string, int> GetTagsCloudBlock(int maxTags = 20)
        {
            Dictionary<string, int> result = new Dictionary<string, int>();
            // Counts number of tags added (we only need max of %maxTags% tags).
            int count = 0;
            int total = maxTags < applicationContext.Tags.Count() ? maxTags : applicationContext.Tags.Count();
            ViewBag.Links = new Dictionary<string, int>();
            foreach (var tag in applicationContext.Tags
                        .OrderByDescending(x => x.ContentTags.Count))
            {
                result.Add(tag.Word, count * 5 / (total));
                ViewBag.Links.Add(tag.Word, tag.Id);
                ++count;
                if (count >= maxTags)
                    break;
            }
            return result;
        }
        
    }
}