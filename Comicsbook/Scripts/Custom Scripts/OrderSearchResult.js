﻿document.getElementById("sort-by-rating").onclick = document.getElementById("sort-by-rating-child").onclick = function (event) {
    document.getElementById("sort-by-date").className = "";
    document.getElementById("sort-by-rating").className = "active";
    if (document.getElementById("search-button") != null) {
        var ind = document.getElementById("search-button").href.indexOf("&order=");
        if (ind != -1) {
            var i = ind
            while (document.getElementById("search-button").href[i] != "=") {
                i++;
            }
            i++;
            var indStart = i;
            var count = 1;
            while (i < document.getElementById("search-button").href.length
                && document.getElementById("search-button").href[i] != "&") {
                count++;
                i++;
            }
            var s = document.getElementById("search-button").href;
            var str = s.substr(indStart, s.length - indStart);
            var rep = str.substr(0, count);
            str = str.replace(rep, "rating");
            s = s.substr(0, indStart);
            s += str;
            document.getElementById("search-button").href = s;
        }
        else {
            document.getElementById("search-button").href += "&order=rating";
        }
    }
}

document.getElementById("sort-by-date").onclick = document.getElementById("sort-by-date-child").onclick = function (event) {
    document.getElementById("sort-by-date").className = "active";
    document.getElementById("sort-by-rating").className = "";
    if (document.getElementById("search-button") != null) {
        var ind = document.getElementById("search-button").href.indexOf("&order=");
        if (ind != -1) {
            var i = ind
            while (document.getElementById("search-button").href[i] != "=") {
                i++;
            }
            i++;
            var indStart = i;
            var count = 1;
            while (i < document.getElementById("search-button").href.length
                && document.getElementById("search-button").href[i] != "&") {
                count++;
                i++;
            }
            var s = document.getElementById("search-button").href;
            var str = s.substr(indStart, s.length - indStart);
            var rep = str.substr(0, count);
            str = str.replace(rep, "date");
            s = s.substr(0, indStart);
            s += str;
            document.getElementById("search-button").href = s;
        }
        else {
            document.getElementById("search-button").href += "&order=date";
        }
    }
}