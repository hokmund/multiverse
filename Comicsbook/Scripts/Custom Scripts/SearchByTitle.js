﻿$(document).ready(function () {
    $(newTag).keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });
});


document.getElementById("articleTitle").onkeyup = function (event) {
    var title = document.getElementById("articleTitle").value;
    if (title == "") {
        title = " ";
    }
        if (document.getElementById("search-button") != null) {
            var ind = document.getElementById("search-button").href.indexOf("&title=");
            if (ind != -1) {
                var i = ind
                while (document.getElementById("search-button").href[i] != "=") {
                    i++;
                }
                i++;
                var indStart = i;
                var count = 1;
                while (i < document.getElementById("search-button").href.length
                    && document.getElementById("search-button").href[i] != "&") {
                    count++;
                    i++;
                }
                var s = document.getElementById("search-button").href;
                var str = s.substr(indStart, s.length - indStart);
                var rep = str.substr(0, count);
                str = str.replace(rep, title);
                s = s.substr(0, indStart);
                s += str;
                document.getElementById("search-button").href = s;
            }
            else {
                document.getElementById("search-button").href += "&title=" + title;
            }
        }
    
}