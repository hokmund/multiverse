﻿// Блокировка отправки формы после ввода тега по нажатию Enter
$(document).ready(function() {
    $(newTag).keydown(function(event){
        if(event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });
    $(newIgnoredTag).keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });
}); 


var selectedTags=[];
document.getElementById("newTag").onkeyup =
    document.getElementById("newIgnoredTag").onkeyup =
    document.getElementById("newTag").onfocus =
    document.getElementById("newIgnoredTag").onfocus =
    function (event)
{

    // Если это ввод не тега для статьи, а тега для игнора в поиске, будем добавлять "Ignored" везде.
    var caseOfIgnoring = "";
    var t = event.currentTarget || event.srcElement
    if (t.id.indexOf("gn") != -1) {
        caseOfIgnoring = "Ignored";
    }
    RemoveListElements(caseOfIgnoring == "Ignored");

    // Получаем значение, написанное в cтроке для тегов.
    var value = document.getElementById("new" + caseOfIgnoring + "Tag").value;

    var lastChar = value[value.length-1];

    // Получаем все теги.
    var tagsElements = [];
    for (var i = 0; document.getElementById("tagWord " + i) != null; ++i)
    {
        tagsElements[i] = document.getElementById("tagWord " + i).value.split(",")[0];
    }

    // Если не нажата запятая, то показываем все возможные теги.
    if(event.keyCode!=13 && lastChar!=',')
    {   

        var arr = [];

        // Добавляенм отфильтрованные элементы списка.
        for(var i = 0; i < tagsElements.length; i++)
        {
            // Отсеиваем ненужные теги.
            if(tagsElements[i].toLowerCase().indexOf(value.toLowerCase()) != -1 && selectedTags.indexOf(tagsElements[i].toLowerCase())==-1){
                arr[i] = tagsElements[i];
                var newLi=document.createElement('li');
                newLi.innerHTML = arr[i];
                newLi.id="li"+i;
                newLi.className="list-group-item";

                newLi.onmouseenter = function(e){
                    e.srcElement.style.backgroundColor = "#F0F8FF";
                }

                newLi.onmouseleave = function(e){
                    e.fromElement.style.backgroundColor="white";
                }

                newLi.onclick = function(e){
                    AddLabel(e.currentTarget.innerHTML, caseOfIgnoring == "Ignored");
                    RemoveListElements(caseOfIgnoring == "Ignored");
                }

                document.getElementById("tag" + caseOfIgnoring + "List").appendChild(newLi);

                // Выпадает не более 5 элементов списка.
                if (i == 4)
                    break;
            }
        }
    }
        // Если нажата запятая или Enter, добавляем тег
    else
    {
                
        if(lastChar==','){
            value = value.substr(0, value.length - 1);
        }

        while (value.indexOf(',') >= 0)
            value = value.replace(",", "");

        value = value.trim();
        if (document.getElementById("tags-input-mode").value == "add" || tagsElements.indexOf(value) > -1)
        {
            if ((value[0] != ' ' && value != "") || value.length > 1)
            {
                // Проверка на уникальность нового тега.
                if (selectedTags.indexOf(value.toLowerCase()) == -1 && tagsElements.indexOf(value.toLowerCase()) == -1)
                {
                    AddLabel(value, caseOfIgnoring == "Ignored");
                }
            }
            else if (value.length > 1)
            {
                value = value.substring(1, value.length - 1);
            }
        }
    }
}


function AddLabel(word, ignored)
{
    var span=document.createElement('span');
    span.id="TagLabels";
    span.name="TagLabels";
    span.style.float = "left";
    span.className = "label label-info btn-xs";
    span.style.marginLeft="10px";
    span.style.marginBottom="5px";
    span.textContent = word;

    var button = document.createElement('button');
    button.type="button";
    button.className="glyphicon glyphicon-remove myclass";
    button.style.color="black";
    button.style.position="relative";
    button.style.left="10px";
    button.style.bottom="-5px";
    button.style.border="none";
    button.style.backgroundColor="transparent";
    button.style.color="white";

    var input = document.createElement('input');
    input.id="tags";
    input.name="tags";
    input.type="hidden";
    input.value=word;

    selectedTags[selectedTags.length] = word.toLowerCase();

    // При нажатии удаляем тэг.
    button.onclick = function(event)
    {
        var tagWord=event.currentTarget.parentElement.value;
        selectedTags.splice(selectedTags.indexOf(tagWord), 1);
        if (!ignored)
        {
            var ul = document.getElementById("selectedTags").removeChild(event.currentTarget.parentElement);

            if (document.getElementById("search-button") != null) {
                for (var i = 0; document.getElementById("tagWord " + i) != null; ++i) {
                    if (document.getElementById("tagWord " + i).value.split(",")[0] == word) {
                        var str = "&tags="+document.getElementById("tagWord " + i).value.split(",")[1];
                        document.getElementById("search-button").href =
                            document.getElementById("search-button").href.replace(str, "");
                    }
                }
            }
        }
        else
        {
            var ul = document.getElementById("ignoredTags").removeChild(event.currentTarget.parentElement);
            if (document.getElementById("search-button") != null) {
                for (var i = 0; document.getElementById("tagWord " + i) != null; ++i) {
                    if (document.getElementById("tagWord " + i).value.split(",")[0] == word) {
                        var str = "&ignoredTags=" + document.getElementById("tagWord " + i).value.split(",")[1];
                        document.getElementById("search-button").href =
                            document.getElementById("search-button").href.replace(str, "");
                    }
                }
            }
        }
    }


    span.appendChild(button);
    span.appendChild(input);

    if (!ignored) {
        document.getElementById("selectedTags").appendChild(span);

        if (document.getElementById("search-button") != null)
        {
            for (var i = 0; document.getElementById("tagWord " + i) != null; ++i)
            {
                if (document.getElementById("tagWord " + i).value.split(",")[0] == word)
                {
                    document.getElementById("search-button").href =
                        document.getElementById("search-button").href +
                        "&tags=" + document.getElementById("tagWord " + i).value.split(",")[1];
                }
            }

        }

        // Очищаем поле ввода.
        document.getElementById("newTag").value = "";
    }
    else {
        document.getElementById("ignoredTags").appendChild(span);
        if (document.getElementById("search-button") != null) {
            for (var i = 0; document.getElementById("tagWord " + i) != null; ++i) {
                if (document.getElementById("tagWord " + i).value.split(",")[0] == word) {
                    document.getElementById("search-button").href =
                        document.getElementById("search-button").href +
                        "&ignoredTags=" + document.getElementById("tagWord " + i).value.split(",")[1];
                }
            }
        }
        // Очищаем поле ввода.
        document.getElementById("newIgnoredTag").value = "";
    }
}

function RemoveListElements(ignored)
{
    var ul;
    // Получаем наш список.
    if (ignored)
        ul = document.getElementById("tagIgnoredList");
    else
        ul = document.getElementById("tagList");

    // Удаляем старые элементы списка.
    while(ul.childElementCount>0)
    {
        ul.removeChild(ul.lastChild);
    }
}
