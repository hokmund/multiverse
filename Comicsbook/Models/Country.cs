﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Comicsbook.Models
{
    public class Country
    {
        [Key]
        public int Id { set; get; }

        [Column]
        public string Word { set; get; }
    }
}