﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Comicsbook.Models
{
    public enum MarkValue { Dislike, Like}

    public class ContentMark
    {
        [Key, Column(Order = 1)]
        [ForeignKey("Author")]
        public string AuthorId { set; get; }
        public virtual ApplicationUser Author { set; get; }

        [Key, Column(Order = 2)]
        [ForeignKey("Content")]
        public int ContentId { set; get; }
        public virtual UserTextContent Content { set; get; }

        [Column]
        public MarkValue Value { get; set; }
    }

    public class CommentMark
    {
        [Key, Column(Order = 1)]
        [ForeignKey("Author")]
        public string AuthorId { set; get; }
        public virtual ApplicationUser Author { set; get; }

        [Key, Column(Order = 2)]
        [ForeignKey("Comment")]
        public int CommentId { set; get; }
        public virtual Comment Comment { set; get; }

        [Column]
        public MarkValue Value { get; set; }
    }
}