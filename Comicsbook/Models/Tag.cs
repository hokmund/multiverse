﻿using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Comicsbook.Models
{
    public class Tag
    {
        ApplicationDbContext applicationContext = new ApplicationDbContext();
        public Tag()
        {
            ContentTags = new List<ContentTag>();
            SubscribedTags = new List<SubscribedTag>();
        }

        [Key]   
        public int Id { set; get; }

        [Column]
        public string Word { set; get; }

        public virtual ICollection<ContentTag> ContentTags { set; get; }
        public virtual ICollection<SubscribedTag> SubscribedTags { set; get; }

    }

    public class SubscribedTag
    {
        [Key, Column(Order = 1)]
        [ForeignKey("User")]
        public string UserId { set; get; }
        public virtual ApplicationUser User { set; get; }

        [Key, Column(Order = 2)]
        [ForeignKey("Tag")]
        public int TagId { set; get; }
        public virtual Tag Tag { set; get; }
    }

    public class IgnoredTag
    {
        [Key, Column(Order = 1)]
        [ForeignKey("User")]
        public string UserId { set; get; }
        public virtual ApplicationUser User { set; get; }

        [Key, Column(Order = 2)]
        [ForeignKey("Tag")]
        public int TagId { set; get; }
        public virtual Tag Tag { set; get; }
    }

    public class ContentTag
    {
        [Key, Column(Order = 1)]
        [ForeignKey("Content")]
        public int ContentId { set; get; }
        public virtual UserTextContent Content { set; get; }

        [Key, Column(Order = 2)]
        [ForeignKey("Tag")]
        public int TagId { set; get; }
        public virtual Tag Tag { set; get; }
    }
}