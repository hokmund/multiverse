﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Comicsbook.Models
{
    public abstract class UserTextContent
    {
        //private static int _id = 0;
        public UserTextContent()
        {
            Marks = new List<ContentMark>();
            Comments = new List<Comment>();
            Tags = new List<ContentTag>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { set; get; }

        [Column]
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Title")]
        public string Title { set; get; }

        [Column]
        [Required]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Text")]
        public string Text { set; get; }

        [ForeignKey("Author")]
        public string AuthorId { set; get; }

        public virtual ApplicationUser Author { set; get; }

        /// <summary>
        /// Дата опубликования статьи.
        /// </summary>
        [Column]
        public DateTime Date { set; get; }

        /// <summary>
        /// Список коментариев к статье.
        /// </summary>
        public virtual ICollection<Comment> Comments { set; get; }

        /// <summary>
        /// Список оценок данной статьи.
        /// </summary>
        public virtual ICollection<ContentMark> Marks { set; get; }

        [Display(Name = "Tags", Prompt = "Enter tags. Divid them by ',' or by 'Enter' keystroke.")]
        public virtual ICollection<ContentTag> Tags { set; get; }

        [NotMapped]
        [Display(Name = "Likes")]
        public int LikesCount
        {
            get
            {
                return Marks.Count(x => x.Value == MarkValue.Like);
            }
        }

        [NotMapped]
        [Display(Name = "Dislikes")]
        public int DislikesCount
        {
            get
            {
                return Marks.Count(x => x.Value == MarkValue.Dislike);
            }
        }

        /// <summary>
        /// Ключ - краткая версия статьи. Значение - картинка для превью
        /// </summary>
        [NotMapped]
        public KeyValuePair<string, string> Preview
        {
            get
            {
                KeyValuePair<string, string> result = new KeyValuePair<string, string>();
                string text = Show(this);

                if (text == null)
                {
                    return new KeyValuePair<string, string>();
                }

                int index = text.IndexOf("[img]");
                string content = string.Empty;
                try
                {
                    if (index != -1)
                    {
                        content = text.Substring(index);
                        content = new String(content.Reverse().ToArray());
                        index = content.LastIndexOf("]gmi/[");
                        content = content.Substring(index);
                        content = new String(content.Reverse().ToArray());

                        while ((index = text.IndexOf("[img]")) != -1)
                        {
                            text = text.Remove(index, text.IndexOf("[/img]") + 6 - index);
                        }
                        while ((index = text.IndexOf("[video]")) != -1)
                        {
                            text = text.Remove(index, text.IndexOf("[/video]") + 8 - index);
                        }
                    }
                    else
                    {
                        index = text.IndexOf("[video]");
                        if (index != -1)
                        {
                            content = text.Substring(index);
                            content = new String(content.Reverse().ToArray());
                            index = content.LastIndexOf("]oediv/[");
                            content = content.Substring(index);
                            content = new String(content.Reverse().ToArray());

                            while ((index = text.IndexOf("[video]")) != -1)
                            {
                                text = text.Remove(index, text.IndexOf("[/video]") + 8 - index);
                            }
                        }
                    }
                }
                catch (IndexOutOfRangeException)
                {

                }
                var temp = text.Take(600);
                text = new String(temp.Reverse().ToArray());
                index = text.IndexOfAny(new char[] { '.', '!', '?' });
                if (index != -1)
                    text = text.Substring(index);
                text = new String(text.Reverse().ToArray());
                result = new KeyValuePair<string, string>(text, content);
                return result;
            }
        }

        [NotMapped]
        public int Rating
        {
            get
            {
                return LikesCount - DislikesCount;
            }
        }

        [NotMapped]
        public ContentType Type 
        { 
            get
            {
                if (this is Meeting)
                {
                    return ContentType.Meeting;
                }
                else if (this is Event)
                {
                    return ContentType.Event;
                }
                else if (this is Article)
                {
                    return ContentType.Article;
                }
                return ContentType.None;
            }
        }

        public static string Show(UserTextContent content)
        {
            string result = content.Text;
            result = result.Replace("watch?v=", "embed/");
            return result;
        }
    }

    public enum ContentType
    {
        Article,
        Event,
        Meeting,
        None
    }
}