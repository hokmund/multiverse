﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Serializable = Multiverse.Models.Serializable;

namespace Comicsbook.Models
{
    public static class ModelConverter
    {
        static ApplicationDbContext context = new ApplicationDbContext();

        public static Serializable.UserTextContent Content(UserTextContent content)
        {
            Serializable.UserTextContent ser = new Serializable.UserTextContent
            {
                Id = content.Id,
                Preview = textConverter(content.Preview.Key),
                Title = content.Title,
                LikesCount = content.LikesCount,
                DislikesCount = content.DislikesCount,
                AuthorUsername = content.Author.UserName,
                Date = content.Date,
                Tags = content.Tags.Select(ctag => new Serializable.Tag
                {
                    Id = ctag.TagId,
                    Word = ctag.Tag.Word
                }).ToList(),
            };

            return ser;
        }

        public static Serializable.Comment Comment(Comment comment)
        {
            Serializable.Comment ser = new Serializable.Comment
            {
                Id = comment.Id,
                LikesCount = comment.LikesCount,
                DislikesCount = comment.DislikesCount,
                //AuthorUsername = comment.Author.UserName,
                AuthorUsername = context.Users.FirstOrDefault(u => u.Id == comment.AuthorId).UserName,
                Date = comment.Date, 
                CommentAnswerId = comment.CommentAnswerId,
                CommentText = textConverter(comment.CommentText)
            };

            return ser;
        }

        public static Serializable.ExtendedContentInfo ExtendedContentInfo(Serializable.ExtendedContentInfo extendedContentInfo)
        {
            Serializable.ExtendedContentInfo ser = new Serializable.ExtendedContentInfo
            {
                Comments = extendedContentInfo.Comments,
                Text = extendedContentInfo.Text,
                CommentsCount = extendedContentInfo.CommentsCount
            };

            return ser;
        }

        /// <summary>
        /// Returns ExtendedContent object with 10 last comments.
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        public static Serializable.ExtendedContentInfo ExtendedContent(UserTextContent content)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            ApplicationUser user = context.Users.First(u => u.Id == content.AuthorId);
            Serializable.ExtendedContentInfo ser = null;

            if (content.Comments.Count > 10)
            {
                ser = new Serializable.ExtendedContentInfo
                {

                    Comments = content.Comments.Skip(content.Comments.Count - 10).Select(comment =>
                    new Serializable.Comment
                    {
                        AuthorUsername = context.Users.FirstOrDefault(u => u.Id == comment.AuthorId).UserName,
                        CommentText = textConverter(comment.CommentText),
                        Date = comment.Date,
                        LikesCount = comment.LikesCount,
                        DislikesCount = comment.DislikesCount,
                        Id = comment.Id
                    }),
                    Text = textConverter(content.Text),
                    CommentsCount = content.Comments.Count
                };
            }
            else
            {
                ser = new Serializable.ExtendedContentInfo
                {
                    Comments = content.Comments.Select(comment =>
                    new Serializable.Comment
                    {
                        AuthorUsername = context.Users.FirstOrDefault(u => u.Id == comment.AuthorId).UserName,
                        CommentText = textConverter(comment.CommentText),
                        Date = comment.Date,
                        LikesCount = comment.LikesCount,
                        DislikesCount = comment.DislikesCount,
                        Id = comment.Id
                    }),
                    Text = textConverter(content.Text),
                    CommentsCount = content.Comments.Count
                };
            }

            return ser;
        }
    
        public static Serializable.User User(ApplicationUser user)
        {
            if (user == null)
            {
                return new Serializable.User();
            }

            return new Serializable.User
            {
                Birthday = user.Birthday,
                City = user.City,
                Country = user.Country,
                Name = user.Name,
                PostsCount = user.Content.Count,
                Rating = user.Rating,
                Sex = (Serializable.Sex)user.Sex,
                Surname = user.Surname,
                Username = user.UserName,
                Userpic = user.Userpic.Image,
            };
        }

        static string textConverter(string text)
        {
            return text.Replace("\"", "'");
        }
    }
}