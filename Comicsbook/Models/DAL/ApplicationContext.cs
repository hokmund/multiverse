﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Comicsbook.Models.DAL
{
    public class ApplicationContext : DbContext
    {
        public DbSet<Article> Articles { set; get; }
        public DbSet<Comment> Comments { set; get; }
    }
}