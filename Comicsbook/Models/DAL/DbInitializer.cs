﻿using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace Comicsbook.Models.DAL
{
    public class DbInitializer : CreateDatabaseIfNotExists<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext db)
        {
            string commentSample1 = "Jackson has long cited several films as influences. It is well known that Jackson has a passion for King Kong, often citing it as his favourite film and as the film that inspired him early in his life. Jackson recalls attempting to remake King Kong when he was 12. At the 2009 San Diego Comic-Con International, while being interviewed alongside Avatar and Titanic director James Cameron, Jackson said certain films gave him a 'kick'. He mentioned Martin Scorsese's crime films Goodfellas and Casino, remarking on 'something about those particular movies and the way Martin Scorsese just fearlessly rockets his camera around and has shot those films that I can watch those movies and feel inspired.' Jackson said the 1970 film Waterloo inspired him in his youth.";
            string commentSample2 = string.Empty;
            string articleSample = "The Walking Dead season 6 got off to an action-packed start last week that could prove to be the demise of Rick and Alexandria. What should you expect next week? AMC has released a new trailer and a sneak peek at the next episode:[video]https://www.youtube.com/embed/_to1vH3QOvg[/video]It was confirmed through a leaked trailer from SDCC 2015 that (as we predicted months ago), The Walking Dead season 6 will premiere on Oct. 11 at 9 pm with a 90-minute premiere, with The Talking Dead to follow at 10:30. AMC will also host the premiere at Madison Square Garden. You can find more details [url=http://www.amc.com/shows/the-walking-dead]here[/url].";
            string sw = "[img]http://www.cinemablend.com/images/news_img/70016/star_wars_70016.jpg[/img]How much do you show? And when do you show it? These are just a few of the questions that J.J. Abrams admits he is wrestling with behind the scenes as the team figures out how best to promote Star Wars: The Force Awakens. Basically, J.J. Abrams knows that they could go into 'radio silence' from now until December, when the sequel opens, and they’d still break box office records. Star Wars, like most of the Marvel properties, doesn’t need to boost the brand. The audience is ready… and many would argue that the less they are shown, the better. In fact, during a conversation with the Irish Examiner during a US-Ireland Alliance Party in Southern California, the director explained that they have TOO MUCH to show, and they don’t want to rush to reveal everything:[quote]We’re just trying to make sure it’s as good as it could be before we put anything out. The disaster would be to race something out for the sake of making an impact and it being not as good as people deserve. … We don’t want to be coy but we have almost a year until the movie comes out, so we have to be smart about what we say and when. We’re always having conversations about it.[/quote]";
            string gotpiano = "[video]https://www.youtube.com/watch?v=OBMEwKeVFSg[/video]Here you go! An applicature for a beautiful piano cover to Game of Thrones main theme.";
            string commentSample3 = "It was quite interesting";
            string commentSample4 = "It's nice to see such a pretty little facts here :)";
            string commentSample5 = "[video]https://www.youtube.com/watch?v=Ac3a49-sj3A[/video] The funniest and the cutest thing i ve ever seen: Flash cast singing gospel version of Ballad of Serenity from the Firefly. Enjoy it!";
        
            var user1 = new ApplicationUser
            {
                UserName = "Ilia",
                Email = "qqq@qqq.com",
            };
            var user2 = new ApplicationUser
            {
                UserName = "Dima",
                Email = "www@www.com",
            };
            var user3 = new ApplicationUser
            {
                UserName = "admin",
                Email = "t@c.com",
            };

            db.Users.Add(user1);
            db.Users.Add(user2);

            Article article4 = new Article { AuthorId = user1.Id, Date = DateTime.Now, Text = commentSample5, Title = "Ballad of Serenity", Comments = new List<Comment>() };
            Article article1 = new Article { AuthorId = user1.Id, Date = DateTime.Now, Text = commentSample1, Title = "About Peter Jackson", Comments = new List<Comment>()};
            Article article2 = new Article { AuthorId = user2.Id, Date = DateTime.Now, Text = "[img]https://s-media-cache-ak0.pinimg.com/736x/55/ac/ee/55acee57d15ccdb45f90c19088be6fc8.jpg[/img]Kit Harringthon was seen if Belfast, so I can assume that Jon Snow will actually live!.", Title = "Jon Snow (SPOILERS!)", Comments = new List<Comment>() };
            Article articleGot = new Article { AuthorId = user2.Id, Date = DateTime.Now, Text = gotpiano, Title = "Synthesia for GoT main theme", Comments = new List<Comment>() };
            
            Event wdPremiere = new Event
            {
                AuthorId = user1.Id,
                Date = DateTime.Now,
                EventDate = new DateTime(2015, 10, 11),
                Text = articleSample,
                Title = "6th Walking Dead season upcoming"
            };
            Event swPremiere = new Event
            {
                AuthorId = user2.Id,
                Date = DateTime.Now,
                EventDate = new DateTime(2015, 12, 18),
                Text = sw,
                Title = "New era of Star Wars"
            };


            db.Contents.Add(article4);
            db.SaveChanges();
            db.Contents.Add(swPremiere);
            db.SaveChanges();
            db.Contents.Add(wdPremiere);
            db.SaveChanges();
            db.Contents.Add(article1);
            db.SaveChanges();
            db.Contents.Add(article2);
            db.SaveChanges();
            db.Contents.Add(articleGot);
            db.SaveChanges();

            Tag tag1 = new Tag
            {
                Word = "Lord of the Rings"
            };
            Tag tag2 = new Tag
            {
                Word = "Firefly"
            };
            Tag tag3 = new Tag
            {
                Word = "GoT"
            };
            Tag tag4 = new Tag
            {
                Word = "The Walking Dead"
            };
            Tag tag5 = new Tag
            {
                Word = "Marvel"
            };
            Tag tag6 = new Tag
            {
                Word = "Flash"
            };

            db.Tags.Add(tag1);
            db.Tags.Add(tag2);
            db.Tags.Add(tag3);
            db.Tags.Add(tag4);
            db.Tags.Add(tag5);
            db.Tags.Add(tag6);
            db.SaveChanges();

            var stag1 = new SubscribedTag { TagId = tag1.Id, User = user1 };
            var stag2 = new SubscribedTag { TagId = tag2.Id, User = user2 };
            var stag3 = new SubscribedTag { TagId = tag3.Id, User = user1 };

            db.SubscribedTags.Add(stag1);
            db.SubscribedTags.Add(stag2);
            db.SubscribedTags.Add(stag3);
            db.SaveChanges();

            var atag1 = new ContentTag { TagId = tag1.Id, ContentId = article1.Id };
            var atag2 = new ContentTag { TagId = tag2.Id, ContentId = article4.Id };
            var atag3 = new ContentTag { TagId = tag3.Id, ContentId = article2.Id };
            var atag5 = new ContentTag { TagId = tag3.Id, ContentId = articleGot.Id };
            var atag4 = new ContentTag { TagId = tag4.Id, ContentId = wdPremiere.Id };
            var atag6 = new ContentTag { TagId = tag6.Id, ContentId = article4.Id };
            db.ContentTags.Add(atag1);
            db.ContentTags.Add(atag2);
            db.ContentTags.Add(atag3);
            db.ContentTags.Add(atag4);
            db.ContentTags.Add(atag5);
            db.ContentTags.Add(atag6);

            Comment comment1 = new Comment { ContentId = article1.Id, AuthorId = user1.Id, Date = DateTime.Now, CommentText = commentSample3 };

            db.Comments.Add(comment1);
            db.SaveChanges();

            Comment comment2 = new Comment { ContentId = article1.Id, AuthorId = user2.Id, CommentAnswerId = comment1.Id, Date = DateTime.Now, CommentText = commentSample4 };
            db.Comments.Add(comment2);
            db.SaveChanges();

            ContentMark mark1 = new ContentMark { ContentId = article1.Id, AuthorId = user1.Id, Value = MarkValue.Like };
            ContentMark mark2 = new ContentMark { ContentId = article2.Id, AuthorId = user1.Id, Value = MarkValue.Like };
            ContentMark mark3 = new ContentMark { ContentId = article1.Id, AuthorId = user2.Id, Value = MarkValue.Dislike };
            ContentMark mark4 = new ContentMark { ContentId = article2.Id, AuthorId = user2.Id, Value = MarkValue.Like };

            CommentMark cmark1 = new CommentMark { CommentId = comment1.Id, AuthorId = user2.Id, Value = MarkValue.Dislike };
            CommentMark cmark2 = new CommentMark { CommentId = comment2.Id, AuthorId = user1.Id, Value = MarkValue.Like };

            db.ContentMarks.Add(mark1);
            db.ContentMarks.Add(mark2);
            db.ContentMarks.Add(mark3);
            db.ContentMarks.Add(mark4);

            db.CommentMarks.Add(cmark1);
            db.CommentMarks.Add(cmark2);

            Event event1 = new Event
            {
                AuthorId = user1.Id,
                Date = DateTime.Now,
                EventDate = new DateTime(2015, 10, 14),
                Text = "New year",
                Title = "New year",
            };

            db.Contents.Add(event1);

            Meeting meeting1 = new Meeting
            {
                AuthorId = user1.Id,
                Date = DateTime.Now,
                EventDate = new DateTime(2016, 1, 1),
                Text = "First Meeting",
                Title = "New year",
                GeoLong = "50.014958",
                GeoLat = "36.227685"
            };

            db.Contents.Add(meeting1);

            //HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().CreateAsync(user3, "admin1");

            //Дбавляем список стран в БД.
            string stringCountries = " Not Selected, Afghanistan, Albania, Algeria, Andorra, Angola, Antigua & Deps, Argentina, Armenia, Australia, Austria, Azerbaijan, Bahamas, Bahrain, Bangladesh, Barbados, Belarus, Belgium, Belize, Benin, Bhutan, Bolivia, Bosnia Herzegovina, Botswana, Brazil, Brunei, Bulgaria, Burkina, Burma, Burundi, Cambodia, Cameroon, Canada, Cape Verde, Central African Rep, Chad, Chile, People's Republic of China, Republic of China, Colombia, Comoros, Democratic Republic of the Congo, Republic of the Congo, Costa Rica,, Croatia, Cuba, Cyprus, Czech Republic, Danzig, Denmark, Djibouti, Dominica, Dominican Republic, East Timor, Ecuador, Egypt, El Salvador, Equatorial Guinea, Eritrea, Estonia, Ethiopia, Fiji, Finland, France, Gabon, Gaza Strip, The Gambia, Georgia, Germany, Ghana, Greece, Grenada, Guatemala, Guinea, Guinea-Bissau, Guyana, Haiti, Holy Roman Empire, Honduras, Hungary, Iceland, India, Indonesia, Iran, Iraq, Republic of Ireland, Israel, Italy, Ivory Coast, Jamaica, Japan, Jonathanland, Jordan, Kazakhstan, Kenya, Kiribati, North Korea, South Korea, Kosovo, Kuwait, Kyrgyzstan, Laos, Latvia, Lebanon, Lesotho, Liberia, Libya, Liechtenstein, Lithuania, Luxembourg, Macedonia, Madagascar, Malawi, Malaysia, Maldives, Mali, Malta, Marshall Islands, Mauritania, Mauritius, Mexico, Micronesia, Moldova, Monaco, Mongolia, Montenegro, Morocco, Mount Athos, Mozambique, Namibia, Nauru, Nepal, Newfoundland, Netherlands, New Zealand, Nicaragua, Niger, Nigeria, Norway, Oman, Ottoman Empire, Pakistan, Palau, Panama, Papua New Guinea, Paraguay, Peru, Philippines, Poland, Portugal, Prussia, Qatar, Romania, Rome, Russian Federation, Rwanda, St Kitts & Nevis, St Lucia, Saint Vincent & the, Grenadines, Samoa, San Marino, Sao Tome & Principe, Saudi Arabia, Senegal, Serbia, Seychelles, Sierra Leone, Singapore, Slovakia, Slovenia, Solomon Islands, Somalia, South Africa, Spain, Sri Lanka, Sudan, Suriname, Swaziland, Sweden, Switzerland, Syria, Tajikistan, Tanzania, Thailand, Togo, Tonga, Trinidad & Tobago, Tunisia, Turkey, Turkmenistan, Tuvalu, Uganda, Ukraine, United Arab Emirates, United Kingdom, United States of America, Uruguay, Uzbekistan, Vanuatu, Vatican City, Venezuela, Vietnam, Yemen, Zambia, Zimbabwe";
            string[] countries = stringCountries.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            int i = 1;
            foreach (string country in countries)
            {
                // Удаляем первый символ (пробел) в слове.
                string _country = country.Substring(1, country.Length - 1);

                db.Countries.Add(new Country { Id = i, Word = _country });
                i++;
            }

            base.Seed(db);
        }
    }
}