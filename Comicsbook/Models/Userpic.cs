﻿using System.IO;

namespace Comicsbook.Models
{
    public class Userpic
    {
        static Userpic ()
	    {
            DEFAULT_USERPIC = Path.Combine(" ", "Pictures", "Userpics", "default.png");
	    }

        private static readonly string DEFAULT_USERPIC;
        private string _image;

        public string Image 
        { 
            get
            {
                return _image ?? DEFAULT_USERPIC;
            }
            set
            {
                _image = value;
            }
        }
        public bool IsDefault 
        { 
            get
            {
                return _image == null || _image == DEFAULT_USERPIC;
            }
        }
    }
}