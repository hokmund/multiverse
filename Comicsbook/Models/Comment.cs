﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Comicsbook.Models
{
    public class Comment
    {
        public Comment()
        {
            Marks = new List<CommentMark>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { set; get; }

        [Column]
        [Required]
        [MinLength(1)]
        [DataType(DataType.MultilineText)]
        public string CommentText { set; get; }

        [ForeignKey("Author")]
        public string AuthorId { get; set; }
        public ApplicationUser Author { set; get; }

        /// <summary>
        /// Дата опубликования комментария.
        /// </summary>
        [Column]
        public DateTime Date { set; get; }

        /// <summary>
        /// Сатья, к которой относится данный комментарий.
        /// </summary>
        [ForeignKey("Content")]
        public int ContentId { get; set; }
        public UserTextContent Content { set; get; }

        /// <summary>
        /// Комментарий, на который отвчает данный комментарий.
        /// Если он не отвечает ни на чей комментарий, то значение этого поля равно null.
        /// </summary>
        [Column]
        public int? CommentAnswerId { set; get; }

        public virtual ICollection<CommentMark> Marks { set; get; }
        
        [NotMapped]
        public int LikesCount
        {
            get
            {
                return Marks.Where(x => x.Value == MarkValue.Like).Count();
            }
        }

        [NotMapped]
        public int DislikesCount
        {
            get
            {
                return Marks.Where(x => x.Value == MarkValue.Dislike).Count();
            }
        }

        [NotMapped]
        public int Rating
        {
            get
            {
                return LikesCount - DislikesCount;
            }
        }
    }
}