﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Comicsbook.Models
{
    public class Meeting: Event
    {
        public Meeting(): base()
        {
            SubscribedMeetings = new List<SubscribedMeeting>();
        }

        /// <summary>
        /// Долгота места проведения - для карт Google.
        /// </summary>
        [Column]
        //[Required(ErrorMessage = "Put the place of meeting at the map.")]
        public string GeoLong { get; set; }

        /// <summary>
        /// Широта места проведения - для карт Google.
        /// </summary>
        [Column]
        //[Required(ErrorMessage="Put the place of meeting at the map.")]
        public string GeoLat { get; set; }

        public virtual ICollection<SubscribedMeeting> SubscribedMeetings { set; get; }
    }

    public class SubscribedMeeting
    {
        [Key, Column(Order = 1)]
        [ForeignKey("User")]
        public string UserId { set; get; }
        public virtual ApplicationUser User { set; get; }

        [Key, Column(Order = 2)]
        [ForeignKey("Meeting")]
        public int MeetingId { set; get; }
        public virtual Meeting Meeting { set; get; }
    }
}