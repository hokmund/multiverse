﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Comicsbook.Models
{
    public class Event : UserTextContent
    {
        public Event() : base()
        {
            SubscribedEvents = new List<SubscribedEvent>();
        }

        [Column]
        //[Required(ErrorMessage="Specify the date of event.")]
        //[DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = "{0:dd'/'MM'/'yyyy}", ApplyFormatInEditMode = true)]
        //[Display(Name = "Date of event")]
        public DateTime? EventDate { get; set; }

        public virtual ICollection<SubscribedEvent> SubscribedEvents { set; get; }
    }

    public class SubscribedEvent
    {
        [Key, Column(Order = 1)]
        [ForeignKey("User")]
        public string UserId { set; get; }
        public virtual ApplicationUser User { set; get; }

        [Key, Column(Order = 2)]
        [ForeignKey("Event")]
        public int EventId { set; get; }
        public virtual Event Event { set; get; }
    }
}