﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Comicsbook.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser() : base()
        {
            Comments = new List<Comment>();
            Content = new List<UserTextContent>();
            CommentMarks = new List<CommentMark>();
            ArticleMarks = new List<ContentMark>();
            SubscribedTags = new List<SubscribedTag>();
            IgnoredTags = new List<IgnoredTag>();
            Userpic = new Userpic();
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            return userIdentity;
        }    

        [Column]
        public string Country { set; get; }
        [Column]
        public string City { set; get; }
        [Column]
        public string Name { set; get; }
        [Column]
        public string Surname { set; get; }
        [Column]
        public Sex Sex { set; get; }
        [Column]
        [DataType(DataType.Date)]
        public DateTime? Birthday { set; get; }
        [Column]
        public List<string> VkRefs { set; get; }

        [Column]
        public string Image 
        {
            get
            {
                return Userpic.Image;
            }
        }

        public Userpic Userpic { set; get; }

        [NotMapped]
        public int Rating
        {
            get
            {
                int articlesRating = Content.Aggregate(0, (seed, article) => seed + article.Rating);
                int commentsRating = Comments.Aggregate(0, (seed, comment) => seed + comment.Rating);

                return articlesRating * 4 + commentsRating;
            }
        }

        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<UserTextContent> Content { set; get; }
        public virtual ICollection<ContentMark> ArticleMarks { set; get; }
        public virtual ICollection<CommentMark> CommentMarks { set; get; }
        public virtual ICollection<SubscribedTag> SubscribedTags { set; get; }
        public virtual ICollection<IgnoredTag> IgnoredTags { set; get; }
        public virtual ICollection<SubscribedEvent> SubscribedEvents { set; get; }
        public virtual ICollection<SubscribedMeeting> SubscribedMeetings { set; get; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public object locker;

        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            //Articles = new DbSet<Article>();
            locker = new object();
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<UserTextContent> Contents { set; get; }
        //public DbSet<Event> Events { get; set; }

        public DbSet<Comment> Comments { set; get; }
        public DbSet<ContentMark> ContentMarks { set; get; }
        public DbSet<CommentMark> CommentMarks { set; get; }
        public DbSet<Tag> Tags { set; get; }
        public DbSet<SubscribedTag> SubscribedTags { set; get; }
        public DbSet<IgnoredTag> IgnoredTags { set; get; }
        public DbSet<ContentTag> ContentTags { set; get; }
        public DbSet<Country> Countries { set; get; }
        public DbSet<SubscribedEvent> SubscribedEvents { get; set; }
        public DbSet<SubscribedMeeting> SubscribedMeetings { set; get; }
        public DbSet<SubscribedUser> SubscribedUsers { set; get; }

        public IQueryable<Article> Articles 
        { 
            get
            {
                var articles = from content in Contents
                                where content is Article
                                select content as Article;
                return articles;
            }
        }

        public IQueryable<Event> Events
        {
            get
            {
                var events = from content in Contents
                             where content is Event && !(content is Meeting)
                             select content as Event;
                return events;
            }
        }

        public IQueryable<Meeting> Meetings
        {
            get
            {
                var meetings = from content in Contents
                               where content is Meeting
                               select content as Meeting;
                return meetings;
            }
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Article>().Map(m =>
            {
                //m.MapInheritedProperties();
                m.ToTable("Contents");
            });

            modelBuilder.Entity<Event>().Map(m =>
            {
                //m.MapInheritedProperties();
                m.ToTable("Contents");
            });

            modelBuilder.Entity<Meeting>().Map(m =>
            {
                m.ToTable("Contents");
            });
        }
    }

    public enum Sex
    { Male, Female }

    public class SubscribedUser
    {
        [Key, Column(Order = 1)]
        //[ForeignKey("SubscribedAuthor")]
        public string SubscribedAuthorId { set; get; }
        //public virtual ApplicationUser SubscribedAuthor { set; get; }

        [Key, Column(Order = 2)]
        //[ForeignKey("Subscriber")]
        public string SubscriberId { set; get; }
        //public virtual ApplicationUser Subscriber { set; get; }
    }
}