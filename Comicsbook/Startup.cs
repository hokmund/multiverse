﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Comicsbook.Startup))]
namespace Comicsbook
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
